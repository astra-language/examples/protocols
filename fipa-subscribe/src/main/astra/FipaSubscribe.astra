agent FipaSubscribe {
    module System system;
    module Console console;
    
    types fr {
        formula topic(string);
        formula channel(string);
        formula subscribed(string, string);
        formula cancelled_topic(string);
        formula content(string, funct);
    }
    
    //***********************************************************************************
    // Participant Rules
    //***********************************************************************************
    goal +!monitor_subscriptions() <: false {
        body {
            console.println("Monitoring for incoming subscriptions");
        }
        
        rule @message(subscribe, string sender, topic(string channel)) : channel(channel) {
            !evaluate_subscription(channel, boolean result);
            if (result == true) {
                send(agree, sender, topic(channel));
                +subscribed(sender, channel);
            } else {
                send(refuse, sender, topic(channel));
            }
        }

        rule @message(subscribe, string sender, topic(string channel)) {
            send(refuse, sender, topic(channel));
        }
        
        rule @message(cancel, string sender, topic(string channel)) : subscribed(sender, channel) {
            -subscribed(sender, channel);
            send(inform, sender, cancelled_topic(channel));
        }

        rule @message(cancel, string sender, topic(string channel)) {
            send(failure, sender, cancelled_topic(channel));
        }
    }

    rule +!evaluate_subscription(string channel, boolean result) {
        result = true;
    }
    

    rule +!publish(string channel, funct message) : channel(channel) {
        foreach(subscribed(string agtId, channel)) {
            send(inform, agtId, content(channel, message));
        }
    }

    //***********************************************************************************
    // Initiator Rules
    //***********************************************************************************
    goal +!subscribe(string target, string channel) <: false {
        body {
            send(subscribe, target, topic(channel));
        }
        
        rule @message(agree, target, topic(channel)) {
            +subscribed(target, channel);
            !subscription_accepted(channel);
        }
        
        rule @message(refuse, target, topic(channel)) {
            !subscription_refused(channel);
            done;
        }
        
        rule @message(failure, target, topic(channel)) {
            system.fail();
        }
        
        rule @message(inform, target, content(channel, funct message)) {
            !handle_content(channel,message);
        }

        rule @message(inform, target, cancelled_topic(channel)) {
            !subscription_cancelled(channel);
            done;
        }
    }
    
    rule +!subscription_accepted(string channel) {console.println("subscription accepted: " + channel);}
    
    rule +!subscription_refused(string channel) {console.println("subscription refused: " + channel);}

    rule +!subscription_cancelled(string channel) {console.println("subscription cancelled: " + channel);}
    
    rule +!handle_content(string channel, funct message) {console.println("content from '" + channel + "': " + message);}

    rule +!cancel_subscription(string target, string channel) : subscribed(target, channel) {
        send(cancel, target, topic(channel));
    }
}
